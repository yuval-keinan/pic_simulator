/**************************************************************************/
// Ver 000  02.02.2020 first version _basic fuctinality:						//
//							 1 UART function 						//
//							 2 Parallel to DAC		  				//
//							 3 DIP switch controll			 		//
//							 4 pushbutton fuctinality  				//
/**************************************************************************/
#include <main_ECG_Simulator.h>
#include "ECGFile.c"

#use fast_io(A)
#use fast_io(B)
#use fast_io(C)

//#define SPI      1

#use rs232( BAUD=115200,xmit=PIN_B14, rcv=PIN_B15)   //

#define Debug/// remove this line for normal operation  

//-------------- global variables-------------------///

int1 flag_1_sec;
int1 flag_1_msec;
int1 File_sent;

unsigned int8 array_size_lo,array_size_hi;
unsigned int16 ECG_data_array[3000];  // 8bit array
unsigned int8 switchConfig;
unsigned int8 buttonConfig,button_state;
unsigned int8 reg_val = 0;
unsigned int8 read_from_eeprom;
unsigned int16 array_size;

// for debug
//unsigned int16 aa;
//unsigned int16 bb;
// unsigned int8 a;
// unsigned int8 b;

// ------ functions recall -----------------------------------------//
void initialize(void);
void base_timer(void);
void command_received(void)  ;
void write_DAC(int8 DAC_val);
int8 DAC_out(void);
unsigned int8 check_switch_configuration(void);
void init_AT25M01 (void);
int8 read_AT25M01_register (int8 addr);
int8 read_AT25M01_byte (int32 addr);
void write_AT25M01_byte (int32 addr,int8 data);
//--------------------------------------------------------------------------------------------//
#include "ECG_simulator_init.c"
//--------------------------------------------------------------------------------------------//
#int_timer1          // timer 1 used to schedule 100 uSEC //1050 hz 952.38uSec 
void base_timer(void)                                   
{   
	static unsigned int16 counter_1_sec;
	static unsigned int8  counter_5_sec,counter_10_sec,counter_20_sec;
	set_timer1(63536);     // 65536 :  8Mhz/4 = 2Mhz => 500nsec between orders
	flag_1_msec = TRUE;
	++counter_1_sec;
//	if (counter_1_sec == 1000)
//	{
//		counter_1_sec = 0;
//		flag_1_sec = TRUE;
//		output_toggle(LED_1);
//		counter_5_sec++;
//		if (counter_5_sec == 4)
//		{
//			counter_5_sec = 0;
//			output_toggle(LED_2);
//			counter_10_sec++;
//			if (counter_10_sec == 2)
//			{
//				counter_10_sec = 0;
//				output_toggle(LED_3);
//				counter_20_sec++;
//				if (counter_20_sec == 3)
//				{
//					counter_20_sec = 0;
//					output_toggle(LED_4);
//				}				
//			}
//		}
//	}
}
//------------------interrupt service routine for UART -------------------------------//
#int_rda    
void command_received(void)  
{  
	static int8 state;
//	static int8 command_state;
	static int8 array_size_counter;
	static int1 ECG_ready_out ;

  	
  	switch (state)
	{  
		case 0: 
			if (rs232in == 'E')
				state = 1;
		        else
		            state = 0;
		break;
		case 1:  
			ECG_ready_out = false;
	        		if (rs232in == 'C')
	            		state = 2;
	        		else
	            		state = 0;
    		break; 
	   	case 2:  
	        		if (rs232in == 'G')
	            		state = 3;
	        		else
	            		state = 0;
    		break; 
	   	case 3: // data stream size 7-0
			array_size_lo = rs232in;
	            	state = 4;
    		break; 
	   	case 4: // data stream size 15-8
			array_size_hi = rs232in;
			array_size = make16(array_size_hi,array_size_lo);
	            	state = 5;
    		break; 
	   	case 5: // data stream 
			if(array_size_counter == array_size)
				state =6;
			else
			{
				ECG_data_array[array_size_counter] = rs232in;
				++array_size_counter;
			}
    		break; 
	   	case 6:
			ECG_ready_out = true;
			state =0;
    		break; 
		
		default:
    		break; 
	}
}

//---------------------------------trasmit signal via DAC--------------------------------//
int8 DAC_out(void)
{
	static int16 wavPtr;
	static int8 val_from_array;
	wavPtr ++ ;
	val_from_array = ECGFile[wavPtr] ;
	if (wavPtr >3000)//	if (wavPtr == sizeof(ECGFile))
	{
		wavPtr = 0;
		File_sent = true ; 
	}
	else
	{
		write_DAC(val_from_array);
		File_sent = false; 
	}
}

//---------------------------------write to parallel DAC--------------------------------//
void write_DAC(int8 DAC_val)
{
	if(bit_test(DAC_val,7))
		output_high(POT_BIT7);
	else
		output_low(POT_BIT7);
	//--
	if(bit_test(DAC_val,6))
		output_high(POT_BIT6);
	else
		output_low(POT_BIT6);
	//--
	if(bit_test(DAC_val,5))
		output_high(POT_BIT5);
	else
		output_low(POT_BIT5);
	//--
	if(bit_test(DAC_val,4))
		output_high(POT_BIT4);
	else
		output_low(POT_BIT4);
	//--
	if(bit_test(DAC_val,3))
		output_high(POT_BIT3);
	else
		output_low(POT_BIT3);
	//--
	if(bit_test(DAC_val,2))
		output_high(POT_BIT2);
	else
		output_low(POT_BIT2);
	//--
	if(bit_test(DAC_val,1))
		output_high(POT_BIT1);
	else
		output_low(POT_BIT1);
	//--
	if(bit_test(DAC_val,0))
		output_high(POT_BIT0);
	else
		output_low(POT_BIT0);
	//--
	output_low(WR); // latch data output
	delay_us(5); 
	output_high(WR); // latch data output

}
//--------------------------------- check switch configuration--------------------------------//
unsigned int8 check_switch_configuration(void)
{
	if(input_state(SW_1)) 	{
		bit_set(switchConfig,0);
		output_low(LED_1);
	}
	else 	{
		bit_clear(switchConfig,0);
		output_toggle(LED_1);
	}
//--
	if(input_state(SW_2)) 	{
		bit_set(switchConfig,1);
		output_low(LED_2);
	}
	else 	{
		bit_clear(switchConfig,1);
		output_toggle(LED_2);
	}
//--
	if(input_state(SW_3)) 	{
		bit_set(switchConfig,2);
		output_low(LED_3);
	}
	else 	{
		bit_clear(switchConfig,2);
		output_toggle(LED_3);
	}
//--
	if(input_state(SW_4)) 	{
		bit_set(switchConfig,3);
		output_low(LED_4);
	}
	else 	{
		bit_clear(switchConfig,3);
		output_toggle(LED_4);
	}
	switchConfig = switchConfig & 0x0F;

return switchConfig;
}
//--------------------------------- check button pushed--------------------------------//
unsigned int8 check_button(void)
{
	if(input_state(P_1)) 	{
		bit_set(buttonConfig,0);
		output_low(LED_1);
	}
	else 	{
		bit_clear(buttonConfig,0);
		output_toggle(LED_1);
	}
//--
	if(input_state(P_2)) 	{
		bit_set(buttonConfig,1);
		output_low(LED_2);
	}
	else 	{	
		bit_clear(buttonConfig,1);
		output_toggle(LED_2);
	}
//--
	if(input_state(P_3)) 	{
		bit_set(buttonConfig,2);
		output_low(LED_3);
	}
	else 	{
		bit_clear(buttonConfig,2);
		output_toggle(LED_3);
	}
//--
	if(input_state(P_4)) 	{
		bit_set(buttonConfig,3);
		output_low(LED_4);
	}
	else 	{
		bit_clear(buttonConfig,3);
		output_toggle(LED_4);
	}
	buttonConfig = buttonConfig & 0x0F;

return buttonConfig;
}
//--------------------------------comm to eeprom--------------------------------------//
void init_AT25M01 (void)
{
	output_low(CS_EE);
	spi_write(WRDI); 			 // Reset Write Enable Latch
	output_high(CS_EE);

	delay_us(1);

	output_low(CS_EE);
	spi_write(WREN);			// Set Write Enable Latch
	output_high(CS_EE);

	delay_us(1);

	output_low(CS_EE);
	spi_write(WRSR);			// Write Status Register
	spi_write(2);			
	output_high(CS_EE);

	reg_val = 	read_AT25M01_register(RDSR);
}
//--------------------Reg------------------------------------//
int8 read_AT25M01_register (int8 addr)
{
	unsigned int8 Rdata ;
	output_low(CS_EE);
	spi_write(addr);
	Rdata = spi_read(0);
	output_high(CS_EE);

	return Rdata;
}
//------------------------read--------------------------------//
int8 read_AT25M01_byte (int32 addr)
{
	int8 Rdata;

	output_low(CS_EE);
	spi_write(READ_M);
	spi_write(make8(addr,2));
	spi_write(make8(addr,1));
	spi_write(make8(addr,0));
	Rdata = spi_read(0);
	output_high(CS_EE);
	
	return Rdata;
}
//---------------------write---------------------------------//
void write_AT25M01_byte (int32 addr,int8 data)
{
	output_low(CS_EE);
	spi_write(WREN);			// Set Write Enable Latch
	output_high(CS_EE);

	delay_us(1);

	output_low(CS_EE);
	spi_write(	WRITE_M);
	spi_write(make8(addr,2));
	spi_write(make8(addr,1));
	spi_write(make8(addr,0));
	spi_write(data);
	output_high(CS_EE);
}
//--------------------------------Main---------------------------------------------------//
void main()
{
	static int8 DAC_counter = 0; ; 
	disable_interrupts(global);
	initialize(); 	
	enable_interrupts(int_timer1);
	enable_interrupts(global);
	init_AT25M01();
	while(TRUE)
	{
	//	check_button();
		check_switch_configuration();
		DAC_out();
		delay_ms(200);
		write_AT25M01_byte(100,0x55);
		read_from_eeprom = read_AT25M01_byte(100);
//		write_AT25M01(0,0x55);
//		putc(0x55);
//		DAC_counter++;
//		write_DAC(0xFF);
//		output_toggle(LED_2);	
//		if (flag_1_sec)
//		{
//			flag_1_sec = False;
//			flag_1_msec = FALSE; 
//		}		
//		else if (flag_1_msec)
//		{
//			flag_1_msec = FALSE;   
//			{
//			}	
//		}
	}
}
//---------------------------------------------------------------------------------------//