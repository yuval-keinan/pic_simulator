void init_UART(void);
void init_SPI(void);
void initialize(void)
{  	
	output_A(0x0000);
	output_B(0x0000);
	output_C(0x0000);

	set_tris_a(0b1111110101101100); 
	set_tris_b(0b1000011100010011);  
	set_tris_c(0b1111110001000100);  
	//          	     5432109876543210               
      
	setup_oscillator( OSC_INTERNAL, 32000000, 8000000);   // (mode,finetune)
	setup_timer1(TMR_INTERNAL | TMR_DIV_BY_1);// timer 1 used to schedule 100 uSEC //1050 hz 952.38uSec 

	output_high(WR); // default DAC write_enable
	init_UART();
	init_SPI();
}
//-----------------------------------------------------------//
void init_UART(void)
{
	RPOR7 = 14;		// UART TX pin14
	RPINR18  = 0x3F15; 	// UART RX pin15

//!   bit_clear(U1MODE,9); // 
//!   bit_set(U1MODE,11);  // UxRTS pin is in Simplex mode
}
//-----------------------------------------------------------//
void init_SPI(void)
{
	setup_spi(SPI_MASTER | SPI_XMIT_L_TO_H |SPI_L_TO_H );
	setup_spi(SPI_MASTER | SPI_XMIT_L_TO_H |SPI_L_TO_H | 0x8000);
	RPOR12 = 0x0800 | 0x0007;		// SCK + MOSI
	RPINR20 = 0x3F0A;	// MISO
//	SPI1BRGL = 10;   // freq 200KHz
	output_high(CS_EE);
}
