/*************************************************************/
// Header file for project ECG simulator						//
// main_ECG_Simulator.h									//
/*************************************************************/
#include <24FJ128GB204.h>
#device ICD=1

#use delay(internal=32000000,restart_wdt)

#FUSES DEBUG 
#FUSES FRC_PLL 
#FUSES OSCIO
//#FUSES SOSC_DIGITAL
#FUSES NOWDT                      //Watch Dog Timer
//#FUSES WDT_SW                    //Watch Dog Timer PreScalar 1:32
//#FUSES WPOSTS11                 //Watch Dog Timer PostScalar 1:1024
#FUSES CKSFSM                   //Clock Switching is enabled, fail Safe clock monitor is enabled
#FUSES NOBROWNOUT               //No brownout reset
//#FUSES BORV_LOW                 //Brown-out Reset set to lowest voltage
//#FUSES BROWNOUT_NOSL
#FUSES ALTI2C1


//  UART1 address
#word   U1MODE   = 0x500 
#word   U1STA        = 0x502
#word   U1TXREG   = 0x504
#word   U1RXREG   = 0x506
#word   U1BRG       = 0x508
#word   OSCTUN    = 0x748
#word   RPOR7      = 0x3E4    // TX
#word   RPINR18   = 0x3B0   // RX
#word   rs232in =  0x506 // rx register


// SPI1
#word RPOR12 = 0x3EE  	// MOSI - function7 & SCK1 - fuction8
#word RPINR20 = 0x3B4	// MISO

#word SPI1CON1L =  0x300  
#word SPI1CON1H =  0x302  
#word SPI1STATL  = 0x308
#word SPI1STATH  = 0x30A
#word SPI1BUFL  = 0x30C
#word SPI1BUFH  = 0x30E
#word SPI1BRGL  = 0x310
#word SPI1IMSKL  = 0x314
#word SPI1IMSKH  = 0x316
#word SPI1URDTl   = 0x318
#word SPI1URDTH   = 0x31A

// PORT A
#define UART_RTS   	 PIN_A0  
#define UART_CTS	 PIN_A1  
#define P_2             	 PIN_A2 
#define SW_1       	PIN_A3  	
#define INPUT_PIN2       PIN_A4         //POT_BIT2 
#define POT_BIT2	 PIN_A7
#define SW_2	          PIN_A8  
#define POT_BIT3      PIN_A9   
#define CON2	   	PIN_A10  // 

// PORT B
#define ICSP_PGD      PIN_B0  
#define ICSP_PGC      PIN_B1  
#define LED_1   	PIN_B2  
#define LED_2		PIN_B3  
#define SW_3		PIN_B4  
#define POT_BIT7	PIN_B5  
#define POT_BIT1      PIN_B6  
#define POT_BIT0     PIN_B7  
#define SW_4	         PIN_B8
#define P_3         	PIN_B9  
#define MISO	   	PIN_B10  
#define CS_EE	   	PIN_B11  
#define WR	   	PIN_B13  
#define UART_TX       PIN_B14  
#define UART_RX       PIN_B15  

// PORT C
#define LED_3    	PIN_C0  
#define LED_4   	PIN_C1  
#define P_1         	PIN_C2  
#define POT_BIT4      PIN_C3    
#define POT_BIT5      PIN_C4   
#define POT_BIT6	PIN_C5  
#define P_4             	PIN_C6 
#define PP	   	 PIN_C7  // Pacemaker_Pulse
#define MOSI	   	 PIN_C8
#define SCK	   	 PIN_C9

// EEPROM - AT25M01 REGISTERS
#define WREN		0x06  // Set Write Enable Latch
#define WRDI 		0x04  // Reset Write Enable Latch
#define RDSR		0x05  // Read Status Register
#define WRSR		0x01  // Write Status Register
#define READ_M	0x03	  // Read Data from Memory Array
#define WRITE_M	0x02  // Write Data to Memory Array
